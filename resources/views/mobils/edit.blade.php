@extends('adminlte.master')

@section('title')
<h1>Edit Mobil</h1>
@endsection

@section('content')
<form action="/mobils/{{$listmobil->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="merek">merek</label>
        <input type="text" class="form-control" value="{{$listmobil->merek}}" name="merek" id="merek" placeholder="Masukkan merek">
        @error('merek')
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                {{ $message }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="jenis_mobil">Jenis Mobil</label>
        <input type="text" class="form-control" value="{{$listmobil->jenis_mobil}}" name="jenis_mobil" id="jenis_mobil" placeholder="Masukkan jenis_mobil">
        @error('jenis_mobil')
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            {{ $message }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @enderror
    </div>
    
    <div class="form-group">
        <label for="kapasitas">kapasitas</label>
        <input type="text" class="form-control" value="{{$listmobil->kapasitas}}" name="kapasitas" id="kapasitas" placeholder="Masukkan kapasitas">
        @error('kapasitas')
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            {{ $message }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="foto">foto</label>
        <input type="file" class="form-control" value="{{$listmobil->foto}}"  name="foto" id="foto" >
        @error('foto')
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            {{ $message }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
@endsection
























{{-- @extends('adminlte.master')

@section('title')
    Halaman Edit Mobil {{$listmobil->id}}
@endsection

@section('content')
<div>
    
    <form action="/mobils/{{$listmobil->id}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="merek">merek</label>
            <input type="text" class="form-control" name="merek" value="{{$listmobil->merek}}" id="merek"
                placeholder="Masukkan merek">
            @error('merek')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="jenis_mobil">jenis_mobil</label>
            <input type="text" class="form-control" name="jenis_mobil" value="{{$listmobil->jenis_mobil}}" id="jenis_mobil"
                placeholder="Masukkan jenis_mobil">
            @error('jenis_mobil')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="kapasitas">kapasitas</label>
            <input type="text" class="form-control" name="kapasitas" value="{{$listmobil->kapasitas}}" id="kapasitas"
                placeholder="Masukkan kapasitas">
            @error('kapasitas')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="foto">foto</label>
            <input type="file" class="form-control" name="foto" value="{{$listmobil->foto}}" id="foto"
                placeholder="Masukkan foto">
            @error('foto')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>
@endsection --}}