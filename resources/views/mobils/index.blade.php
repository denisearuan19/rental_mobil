@extends('adminlte.master')

@section('title')
<h1>List Mobil</h1>
@endsection

@section('content')
<a href="/mobils/create" class="btn btn-primary mb-2">Tambah</a>

<div class="row">
            @foreach ( $listmobil as $item)
                <div class="col-mb-4">
                    <div class="card-columns-fluid" style="width:15rem;">
                        <img src="{{asset('uploads/mobils/'.$item->foto)}}" class="card-img-top" alt="..." >
                        <div class="card-body">
                            <h4>Merek : {{$item->merek}}</h4><br>
                            <h4>Kapasitas : {{$item->kapasitas}}</h4>
                            <form action="/mobils/{{$item->id}}" method="POST">
                                <a href="/mobils/{{$item->id}}" class="btn btn-info">Show</a>
                                <a href="/mobils/{{$item->id}}/edit" class="btn btn-primary">Edit</a>
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </div>
                    </div> 
                </div>
            @endforeach
</div> 



{{-- 
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">merek</th>
                <th scope="col">jenis_mobil</th>
                <th scope="col">kapasitas</th>
                <th scope="col">foto</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($listmobil as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->merek}}</td>
                        <td>{{$value->jenis_mobil}}</td>
                        <td>{{$value->kapasitas}}</td>
                        <td>{{$value->foto}}</td>
                        <td>
                            <form action="/mobils/{{$value->id}}" method="POST">
                                <a href="/mobils/{{$value->id}}" class="btn btn-info">Show</a>
                                <a href="/mobils/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
     --}}
@endsection